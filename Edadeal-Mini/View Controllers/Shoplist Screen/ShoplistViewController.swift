//
//  ShoplistViewController.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 07/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

class ShoplistViewController: UIViewController {
  
  var itemTableVC: ItemTableViewController!

  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupViewController()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.navigationBar.topItem?.title = "Список покупок"
    self.itemTableVC.tableView.reloadData()
  }
  
  func setupViewController() {
    self.view.backgroundColor = UIColor.white
    self.addTableViewController()
  }
  
  func addTableViewController() {
    self.itemTableVC = ItemTableViewController()
    self.addChildViewController(self.itemTableVC)
    self.itemTableVC.tableView.dataSource = self
    self.itemTableVC.view.translatesAutoresizingMaskIntoConstraints = false
    self.view.addSubview(self.itemTableVC.view)
    self.itemTableVC.view.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor).isActive = true
    self.itemTableVC.view.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor).isActive = true
    self.itemTableVC.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
    self.itemTableVC.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
  }

}

// MARK: UITableViewDataSource extension

extension ShoplistViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return EdadealCore.sharedInstance.getShoppingList().count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableCell", for: indexPath) as! ItemTableViewCell
    let item = EdadealCore.sharedInstance.getShoppingList()[indexPath.row]
    cell.loadData(from: item, imageForButton: #imageLiteral(resourceName: "removeActionIcon"), actionDelegate: self)
    return cell
  }
}

// MARK: ItemTableViewCellDelegate extension

extension ShoplistViewController: ItemTableViewCellDelegate {
  func actionButtonClick(_ forItem: Any!) {
    guard let selectedItem = forItem as? ItemModel else { return }
    let index = EdadealCore.sharedInstance.getShoppingList().index(of: selectedItem)
    EdadealCore.sharedInstance.removeFromShoppingList(item: selectedItem)
    self.itemTableVC.removeCell(index!)
  }
}
