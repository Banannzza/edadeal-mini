//
//  SearchViewController.m
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 07/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

#import "SearchViewController.h"
#import "Edadeal_Mini-Swift.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface SearchViewController () <EdadealCoreDelegate, ItemTableViewCellDelegate>

@end

@implementation SearchViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setupViewController];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  EdadealCore.sharedInstance.delegate = self;
  if (!EdadealCore.sharedInstance.isLoaded) {
    [EdadealCore.sharedInstance getAllItems];
    [SVProgressHUD showWithStatus:@"Загрузка данных"];
  }
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [[[[self navigationController] navigationBar] topItem] setTitle:@"Поиск"];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  dispatch_async(dispatch_get_main_queue(), ^{
    [SVProgressHUD dismiss];
  });
}

- (void)addTableViewController {
  self.itemTableVC = [[ItemTableViewController alloc] init];
  [self addChildViewController:self.itemTableVC];
  self.itemTableVC.tableView.dataSource = self;
  [[self.itemTableVC view] setTranslatesAutoresizingMaskIntoConstraints:NO];
  [self.view addSubview:self.itemTableVC.view];
  if (self.navigationController.navigationBar != nil)
    [[[self.itemTableVC.view topAnchor] constraintEqualToAnchor: [self.navigationController.navigationBar bottomAnchor]] setActive:YES];
  else
    [[[self.itemTableVC.view topAnchor] constraintEqualToAnchor: [self.view topAnchor]] setActive:YES];
  [[[self.itemTableVC.view bottomAnchor] constraintEqualToAnchor:[[self bottomLayoutGuide] topAnchor]] setActive:YES];
  [[[self.itemTableVC.view leadingAnchor] constraintEqualToAnchor:[self.view leadingAnchor]] setActive:YES];
  [[[self.itemTableVC.view trailingAnchor] constraintEqualToAnchor:[self.view trailingAnchor]] setActive:YES];
}

- (void)addSearchBar {
  self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
  [self.searchController setSearchResultsUpdater:self];
  [self.searchController setHidesNavigationBarDuringPresentation:NO];
  [self.searchController setDimsBackgroundDuringPresentation:NO];
  [[self.searchController searchBar] sizeToFit];
  [[self.searchController searchBar] setPlaceholder:@"Поиск"];
  [[self.searchController searchBar] setDelegate:self];
  [[self.searchController searchBar] setHidden:YES];
  [[self.searchController searchBar] setValue:@"Отмена" forKey:@"_cancelButtonText"];
  [[self.itemTableVC tableView] setTableHeaderView:[self.searchController searchBar]];
}

- (void)setupViewController {
  [self.view setBackgroundColor:[UIColor whiteColor]];
  [self addTableViewController];
  [self addSearchBar];
  [SVProgressHUD setBorderWidth:1.0];
  [SVProgressHUD setBorderColor:[UIColor lightGrayColor]];
}


#pragma mark- UITableViewDataSource delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if ([self.searchController isActive])
    return [[EdadealCore.sharedInstance geAllItemsSortedBy:[self.searchController.searchBar text]] count];
  else
    return [EdadealCore.sharedInstance.getAllItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemTableCell" forIndexPath:indexPath];
  ItemModel* item;
  if ([self.searchController isActive])
    item = [[EdadealCore.sharedInstance geAllItemsSortedBy:[self.searchController.searchBar text]] objectAtIndex:indexPath.row];
  else
    item = [EdadealCore.sharedInstance.getAllItems objectAtIndex:indexPath.row];

  [cell loadDataFrom:item imageForButton:[UIImage imageNamed:@"buyActionIcon"] actionDelegate:self];
  [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  return cell;
}


#pragma mark- UISearchResultsUpdating delegate

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
  if (![searchController isActive])
    return;
  [self.itemTableVC.tableView reloadData];
}

#pragma mark- UISearchBar delegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
  [self.itemTableVC.tableView reloadData];
}

#pragma mark- EdadealCore delegate

- (void)itemsDidLoaded {
  [[self.searchController searchBar] setHidden:NO];
  [self.itemTableVC.tableView reloadData];
  dispatch_async(dispatch_get_main_queue(), ^{
    [SVProgressHUD dismiss];
  });
}

- (void)itemLoadErrorWithError:(NSError *)error {
  NSString *errorMessage =  error.localizedDescription == nil ? @"Что-то пошло не так" : error.localizedDescription;
  dispatch_async(dispatch_get_main_queue(), ^{
    [SVProgressHUD dismiss];
    [SVProgressHUD showErrorWithStatus:errorMessage];
  });
}

#pragma mark- ItemTableViewCellDelegate delegate

- (void)actionButtonClick:(id)forItem {
  ItemModel* clickedItem = forItem;
  [EdadealCore.sharedInstance addToShoppigListWithItem:clickedItem];
}
@end
