//
//  SearchViewController.h
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 07/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemTableViewController.h"
#import "ItemTableViewCell.h"

@interface SearchViewController : UIViewController <UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate>

@property ItemTableViewController *itemTableVC;
@property UISearchController *searchController;

@end
