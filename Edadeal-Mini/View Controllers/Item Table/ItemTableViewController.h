//
//  ItemTableViewController.h
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 05/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemTableViewController : UITableViewController

- (void)removeCell:(NSInteger)forRow;

@end



