//
//  ItemTableViewController.m
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 05/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

#import "ItemTableViewController.h"
#import "Edadeal_Mini-Swift.h"

@interface ItemTableViewController ()

@end

@implementation ItemTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.tableView registerClass:[ItemTableViewCell class] forCellReuseIdentifier:@"ItemTableCell"];
  [self.tableView setTableFooterView: [[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)removeCell:(NSInteger)forRow {
  [self.tableView beginUpdates];
  [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:forRow inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
  [self.tableView endUpdates];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemTableCell" forIndexPath:indexPath];
  return cell;
}

#pragma mark- Menu table Delegates

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return UITableViewAutomaticDimension;
}
@end
