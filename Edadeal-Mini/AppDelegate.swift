//
//  AppDelegate.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 05/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      let tabBarController = UITabBarController()
      let searchVC = SearchViewController()
      searchVC.tabBarItem = UITabBarItem(title: "Поиск", image: #imageLiteral(resourceName: "searchBarItemIcon"), tag: 1)
      
      let shoplistVC = ShoplistViewController()
      shoplistVC.tabBarItem = UITabBarItem(title: "Покупки", image: #imageLiteral(resourceName: "shoplistBarItemIcon"), tag: 2)
      tabBarController.viewControllers = [searchVC, shoplistVC]
      
      let navigationVC = UINavigationController(rootViewController: tabBarController)
      self.window?.rootViewController = navigationVC
      self.window?.makeKeyAndVisible()
      return true
    }

}

