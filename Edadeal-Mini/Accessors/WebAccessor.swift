//
//  WebAccessor.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 06/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import Foundation
import Alamofire

class WebAccessor: NSObject {
  
  static func loadItems(fromUrl urlString: String, completionHandler: @escaping (Bool, Error?, [ItemModel]?) -> Void) {
    guard let url = URL(string: urlString) else {
      completionHandler(false, nil, nil)
      return
    }
    request(url, method: .get).responseJSON { responseJSON in
      switch responseJSON.result {
      case .success(let value):
        if let jsonArray = value as? [[String: Any]] {
          let itemList = jsonArray.flatMap({ ItemModel(json: $0) })
          completionHandler(true, nil, itemList)
        }
      case .failure(let error):
        completionHandler(false, error, nil)
        print(error)
      }
    }
  }
  
}
