//
//  DiskAccessor.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 06/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import Foundation


class DisckAcessor: NSObject {
  
  static let defaultKeyForArchivedData = "ArchivedData"
  
  static func save(itemList: [ItemModel], withKey key: String) {
    let archivedData = NSKeyedArchiver.archivedData(withRootObject: itemList)
    UserDefaults.standard.set(archivedData, forKey: key)
  }
  
  static func load(forKey key: String) -> [ItemModel]? {
    guard let archivedData = UserDefaults.standard.object(forKey: key) as? Data,
          let itemList = NSKeyedUnarchiver.unarchiveObject(with: archivedData) as? [ItemModel] else { return nil }
    return itemList
  }
  
}
