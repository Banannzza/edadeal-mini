//
//  ItemModel.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 06/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import Foundation

@objc class ItemModel: NSObject, NSCoding {
  
  @objc public private(set) var itemDescription: String
  @objc public private(set) var retailer: String?
  @objc public private(set) var price: NSNumber?
  @objc public private(set) var discount: NSNumber?
  @objc public private(set) var imageLogoURL: URL?
  
  init(description: String, retailer: String?, price: NSNumber?, discount: NSNumber?, logoURL: URL?) {
    self.itemDescription = description
    self.retailer = retailer
    self.price = price
    self.discount = discount
    self.imageLogoURL = logoURL
  }
  
  convenience init?(json: [String: Any]) {
    guard let description = json["description"] as? String else { return nil }
    let retailer = json["retailer"] as? String
    let price = json["price"] as? NSNumber
    let discount = json["discount"] as? NSNumber
    let imageLogoString = json["image"] as? String
    let imageLogoURL = imageLogoString == nil ? nil : URL(string: imageLogoString!)
    self.init(description: description, retailer: retailer, price: price, discount: discount, logoURL: imageLogoURL)
  }
  
  required convenience init?(coder aDecoder: NSCoder) {
    guard let description = aDecoder.decodeObject(forKey: "description") as? String else { return nil }
    let retailer = aDecoder.decodeObject(forKey: "retailer") as? String
    let price = aDecoder.decodeObject(forKey: "price") as? NSNumber
    let discount = aDecoder.decodeObject(forKey: "discount") as? NSNumber
    let imageLogoURL = aDecoder.decodeObject(forKey: "imageLogoURL") as? URL
    self.init(description: description, retailer: retailer, price: price, discount: discount, logoURL: imageLogoURL)
  }
 
  func encode(with aCoder: NSCoder) {
    aCoder.encode(self.itemDescription, forKey: "description")
    if let retailer = self.retailer {
      aCoder.encode(retailer, forKey: "retailer")
    }
    if let price = self.price {
      aCoder.encode(price, forKey: "price")
    }
    if let discount = self.discount {
      aCoder.encode(discount, forKey: "discount")
    }
    if let imageLogoURL = self.imageLogoURL {
      aCoder.encode(imageLogoURL, forKey: "imageLogoURL")
    }
  }
  
  override func isEqual(_ object: Any?) -> Bool {
    if let secondItem = object as? ItemModel {
      return self == secondItem
    }
    return false
  }
  
  static func ==(left: ItemModel, right: ItemModel) -> Bool {
    return left.itemDescription == right.itemDescription && left.retailer == right.retailer
  }
}


