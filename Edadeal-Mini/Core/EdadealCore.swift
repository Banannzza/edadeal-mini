//
//  EdadealCore.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 07/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import Foundation

@objc class EdadealCore: NSObject {

  private let apiURL = "https://api.edadev.ru/intern/"
  private var shoppingList: [ItemModel]?
  private var allItems: [ItemModel]?
  private var loading: Bool = false
  
  @objc static let sharedInstance = EdadealCore()
  @objc weak var delegate: EdadealCoreDelegate?
  
  private override init() {
    shoppingList = DisckAcessor.load(forKey: DisckAcessor.defaultKeyForArchivedData)
  }
  
  @objc func getShoppingList() -> [ItemModel] {
    return shoppingList ?? []
  }
  
  @objc func removeFromShoppingList(item: ItemModel) {
    guard let itemIndex = self.shoppingList?.index(of: item) else { return }
    self.shoppingList?.remove(at: itemIndex)
    self.saveToDisk(shoppingList: self.shoppingList ?? [])
  }
  
  @objc func addToShoppigList(item: ItemModel) {
    if self.shoppingList?.contains(item) ?? false {
      return
    }
    self.shoppingList?.append(item) ?? (self.shoppingList = [item])
    self.saveToDisk(shoppingList: self.shoppingList ?? [])
  }
  
  @objc func saveToDisk(shoppingList: [ItemModel]) {
    DisckAcessor.save(itemList: shoppingList, withKey: DisckAcessor.defaultKeyForArchivedData)
  }
  
  @objc func isLoaded() -> Bool {
    return self.allItems != nil
  }
  
  @objc @discardableResult func getAllItems() -> [ItemModel] {
    guard !loading else { return [] }
    if let allItems = self.allItems {
      return allItems
    }
    else {
      loading = true
      WebAccessor.loadItems(fromUrl: apiURL, completionHandler: { (success, error, itemList) in
        self.loading = false
        if success {
          self.allItems = itemList
          self.delegate?.itemsDidLoaded?()
        }
        else if let loadError = error {
          self.delegate?.itemLoadError?(error: loadError)
        }
      })
      return []
    }
  }
  
  @objc func geAllItemsSortedBy(_ searchString: String?) -> [ItemModel] {
    guard let items = self.allItems else { return [] }
    guard let searchString = searchString, searchString != "" else { return items }
    return items.filter({$0.itemDescription.range(of: searchString, options: .caseInsensitive) != nil }).sorted(by: { (firstItem, secondItem) -> Bool in
      if let firstSubstringLowerBound = firstItem.itemDescription.range(of: searchString, options: .caseInsensitive)?.lowerBound,
         let secondSubstringLowerBound = secondItem.itemDescription.range(of: searchString, options: .caseInsensitive)?.lowerBound {
        return firstSubstringLowerBound < secondSubstringLowerBound
      }
      return false
    })
  }

}

@objc protocol EdadealCoreDelegate: class {
  @objc optional func itemsDidLoaded()
  @objc optional func itemLoadError(error: Error)
}
