//
//  ItemTableViewCell.m
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 05/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

#import "ItemTableViewCell.h"
#import "Edadeal_Mini-Swift.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ItemTableViewCell

- (void)awakeFromNib {
  [super awakeFromNib];
  [self initializeSubview];
  [self initializeConstraints];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  ItemTableViewCell *cell = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  [cell initializeSubview];
  [cell initializeConstraints];
  return cell;
}

- (void)loadDataFrom:(ItemModel*) item imageForButton:(UIImage*)image actionDelegate:(id<ItemTableViewCellDelegate>)actionDelegate {
  self.item = item;
  self.delegate = actionDelegate;
  [self.actionButton setImage:image forState:UIControlStateNormal];
  [self.descriptionLabel setText: item.itemDescription];
  [self.retailerLabel setText:item.retailer];
  if (item.price != nil)
    [self.priceLabel setText:[NSString stringWithFormat:@"%0.02f", [item.price doubleValue]]];
  if (item.discount != nil)
    [self.discountLabel setText:[NSString stringWithFormat:@"%@%%", item.discount]];
  if (item.imageLogoURL != nil) {
    [self.logoImageView sd_setImageWithURL:item.imageLogoURL];
  }
}

-(void)prepareForReuse {
  [super prepareForReuse];
  [self.descriptionLabel setText:@""];
  [self.retailerLabel setText:@""];
  [self.priceLabel setText:@""];
  [self.discountLabel setText:@""];
  [self.logoImageView setImage:nil];
}

#pragma mark- Subviews initialization

- (void)initializeSubview {
  [self initializeInfoView];
  [self initializePriceDiscountView];
  [self initializationDescriptionLabel];
  [self initializationRetailorLabel];
  [self initializationPriceLabel];
  [self initializationDiscountLabel];
  [self initializeLogoImageView];
  [self initializeActionButton];
}

- (void)addView:(UIView *)view toSuperview:(UIView*)superview{
  [view setTranslatesAutoresizingMaskIntoConstraints:NO];
  [superview addSubview:view];
}

- (void)initializeInfoView {
  self.itemInfoView = [[UIView alloc] init];
  [self.itemInfoView.layer setCornerRadius:5.0];
  [self.itemInfoView setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]];
  [self addView:self.itemInfoView toSuperview:self.contentView];
}

- (void)initializePriceDiscountView {
  assert(self.itemInfoView != nil);
  self.itemPriceDiscountView = [[UIView alloc] init];
//  [self.itemPriceDiscountView setBackgroundColor:[UIColor brownColor]];
  [self addView:self.itemPriceDiscountView toSuperview:self.itemInfoView];
}

- (void)initializeLogoImageView {
  self.logoImageView = [[UIImageView alloc] init];
  [self.logoImageView setContentMode:UIViewContentModeScaleAspectFit];
  [self addView:self.logoImageView toSuperview:self.contentView];
}

- (void)initializeActionButton {
  self.actionButton = [[UIButton alloc] init];
  [self.actionButton addTarget:self action:@selector(actionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
  [self addView:self.actionButton toSuperview:self.contentView];
}

- (void)initializationDescriptionLabel {
  assert(self.itemInfoView != nil);
  self.descriptionLabel = [[UILabel alloc] init];
  [self.descriptionLabel setNumberOfLines:0];
  [self addView:self.descriptionLabel toSuperview:self.itemInfoView];
}

- (void)initializationRetailorLabel {
  assert(self.itemInfoView != nil);
  self.retailerLabel = [[UILabel alloc] init];
  [self.retailerLabel setNumberOfLines:0];
  [self addView:self.retailerLabel toSuperview:self.itemInfoView];
}

- (void)initializationPriceLabel {
  assert(self.itemPriceDiscountView != nil);
  self.priceLabel = [[UILabel alloc] init];
  [self addView:self.priceLabel toSuperview:self.itemPriceDiscountView];
}

- (void)initializationDiscountLabel {
  assert(self.itemPriceDiscountView != nil);
  self.discountLabel = [[UILabel alloc] init];
  [self addView:self.discountLabel toSuperview:self.itemPriceDiscountView];
}


#pragma mark- Constraint initialization

- (void)initializeConstraints {
  [self addConstraintForLogoView];
  [self addConstraintForActionButton];
  [self addConstraintsForInfoView];
  [self addConstraintForDescriptionLabel];
  [self addConstraintForRetailorLabel];
  [self addConstraintForPriceDiscountView];
  [self addConstraintForPriceLabel];
  [self addConstraintForDiscountLabel];
}

- (void)addConstraintForActionButton {
  [[[self.actionButton centerXAnchor] constraintEqualToAnchor:[self.logoImageView centerXAnchor]] setActive:YES];
  [[[self.actionButton topAnchor] constraintEqualToAnchor:[self.logoImageView bottomAnchor] constant:indentationInCell] setActive:YES];
  [[[self.actionButton heightAnchor] constraintEqualToAnchor:[self.logoImageView heightAnchor] multiplier:0.8] setActive:YES];
  [[[self.actionButton widthAnchor] constraintEqualToAnchor:[self.actionButton heightAnchor]] setActive:YES];
  NSLayoutConstraint *constraint = [[self.actionButton bottomAnchor] constraintLessThanOrEqualToAnchor:[[self.contentView layoutMarginsGuide] bottomAnchor]];
  [constraint setPriority:UILayoutPriorityDefaultHigh];
  [constraint setActive:YES];
}

- (void)addConstraintForLogoView {
  [[[self.logoImageView topAnchor] constraintEqualToAnchor:[[self.contentView layoutMarginsGuide] topAnchor]] setActive:YES];
  [[[self.logoImageView trailingAnchor] constraintEqualToAnchor:[[self.contentView layoutMarginsGuide] trailingAnchor]] setActive:YES];
  [[[self.logoImageView heightAnchor] constraintEqualToConstant: [[UIScreen mainScreen] bounds].size.height * 0.1] setActive:YES];
  [[[self.logoImageView widthAnchor] constraintEqualToAnchor:[self.logoImageView heightAnchor]] setActive:YES];
}

- (void)addConstraintsForInfoView {
  [[[self.itemInfoView leadingAnchor] constraintEqualToAnchor:[[self.contentView layoutMarginsGuide] leadingAnchor]] setActive:YES];
  [[[self.itemInfoView trailingAnchor] constraintEqualToAnchor:[self.logoImageView leadingAnchor] constant: -1 * indentationInCell] setActive:YES];
  [[[self.itemInfoView topAnchor] constraintEqualToAnchor:[[self.contentView layoutMarginsGuide] topAnchor]] setActive:YES];
  NSLayoutConstraint *constraint = [[self.itemInfoView bottomAnchor] constraintLessThanOrEqualToAnchor:[[self.contentView layoutMarginsGuide] bottomAnchor]];
  [constraint setPriority:UILayoutPriorityDefaultLow];
  [constraint setActive:YES];
}

- (void)addConstraintForDescriptionLabel {
  [[[self.descriptionLabel leadingAnchor] constraintEqualToAnchor:[self.itemInfoView leadingAnchor] constant:indentationInCell] setActive:YES];
  [[[self.descriptionLabel trailingAnchor] constraintEqualToAnchor:[self.itemInfoView trailingAnchor] constant:-1 * indentationInCell] setActive:YES];
  [[[self.descriptionLabel topAnchor] constraintEqualToAnchor:[self.itemInfoView topAnchor]] setActive:YES];
}

- (void)addConstraintForRetailorLabel {
  [[[self.retailerLabel leadingAnchor] constraintEqualToAnchor:[self.descriptionLabel leadingAnchor]] setActive:YES];
  [[[self.retailerLabel trailingAnchor] constraintEqualToAnchor:[self.descriptionLabel trailingAnchor]] setActive:YES];
  [[[self.retailerLabel topAnchor] constraintEqualToAnchor:[self.descriptionLabel bottomAnchor] constant:indentationInCell] setActive:YES];
}

- (void)addConstraintForPriceDiscountView {
  [[[self.itemPriceDiscountView leadingAnchor] constraintEqualToAnchor:[self.descriptionLabel leadingAnchor]] setActive:YES];
  [[[self.itemPriceDiscountView trailingAnchor] constraintEqualToAnchor:[self.descriptionLabel trailingAnchor]] setActive:YES];
   [[[self.itemPriceDiscountView bottomAnchor] constraintEqualToAnchor:[self.itemInfoView bottomAnchor]] setActive:YES];
  [[[self.itemPriceDiscountView topAnchor] constraintEqualToAnchor:[self.retailerLabel bottomAnchor] constant:indentationInCell] setActive:YES];
}

- (void)addConstraintForPriceLabel {
  [[[self.priceLabel leadingAnchor] constraintEqualToAnchor:[self.itemPriceDiscountView leadingAnchor]] setActive:YES];
  [[[self.priceLabel topAnchor] constraintEqualToAnchor:[self.itemPriceDiscountView topAnchor]] setActive:YES];
  [[[self.priceLabel widthAnchor] constraintLessThanOrEqualToAnchor:[self.itemInfoView widthAnchor] multiplier:0.5] setActive:YES];
  [[[self.priceLabel bottomAnchor] constraintEqualToAnchor:[self.itemPriceDiscountView bottomAnchor]] setActive:YES];
}

- (void)addConstraintForDiscountLabel {
  [[[self.discountLabel trailingAnchor] constraintEqualToAnchor:[self.itemPriceDiscountView trailingAnchor]] setActive:YES];
  [[[self.discountLabel topAnchor] constraintEqualToAnchor:[self.itemPriceDiscountView topAnchor]] setActive:YES];
  [[self.discountLabel widthAnchor] constraintLessThanOrEqualToAnchor:[self.itemPriceDiscountView widthAnchor] multiplier:0.4];
  [[[self.discountLabel bottomAnchor] constraintEqualToAnchor:[self.itemPriceDiscountView bottomAnchor]] setActive:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
}

#pragma mark- Action

- (void)actionButtonClick:(UIButton*)sender {
  [self.delegate actionButtonClick:self.item];
}

@end
