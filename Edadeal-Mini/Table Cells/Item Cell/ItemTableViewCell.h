//
//  ItemTableViewCell.h
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 05/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ItemTableViewCellDelegate
- (void)actionButtonClick:(id)forItem;
@end

@interface ItemTableViewCell : UITableViewCell

@property UILabel *descriptionLabel;
@property UILabel *retailerLabel;
@property UILabel *priceLabel;
@property UILabel *discountLabel;
@property UIView *itemInfoView;
@property UIView *itemPriceDiscountView;
@property UIImageView *logoImageView;
@property UIButton *actionButton;

@property NSObject *item;
@property (nonatomic, weak) id <ItemTableViewCellDelegate> delegate;

- (void)loadDataFrom:(NSObject*)item imageForButton:(UIImage*)image actionDelegate:(id<ItemTableViewCellDelegate>)actionDelegate;

@end
