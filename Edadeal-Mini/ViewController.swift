//
//  ViewController.swift
//  Edadeal-Mini
//
//  Created by Алексей Остапенко on 05/12/2017.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
  
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let vc = ItemTableViewController()
    self.present(vc, animated: true, completion: nil)
  }

}

